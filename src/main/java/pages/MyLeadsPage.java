package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using = "Create Lead")WebElement elecreatelead;
	@FindBy(how =How.LINK_TEXT, using = "Find Leads")WebElement eleFindLeads;
	@FindBy(how = How.LINK_TEXT, using ="Merge Leads")WebElement eleMergeLeads;

	@And("Click on Create Leads in the MyLeads page")
	public CreateLeadsPage clickCreateLeads() {		
		click(elecreatelead);
		return new CreateLeadsPage();
	}
	@And ("Click on Find Leads")
	public FindLeadsPage clickFindLeads() {		
		click(eleFindLeads);
		return new FindLeadsPage();
	}
	
	public MergeLeadPage clickMergeLeads() {		
		click(eleMergeLeads);
		return new MergeLeadPage();
	}

}