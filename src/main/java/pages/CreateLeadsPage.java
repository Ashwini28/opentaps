package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class CreateLeadsPage extends ProjectMethods{

	public CreateLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "createLeadForm_companyName")WebElement elecompanyname;
	@FindBy(id = "createLeadForm_firstName")WebElement elefirstname;
	@FindBy(id ="createLeadForm_lastName") WebElement elelastname;
	@FindBy(id ="createLeadForm_marketingCampaignId") WebElement elemarketingcampaignid;
	@FindBy(how = How.NAME, using = "dataSourceId") WebElement eledatasourceid;
	@FindBy(how = How.CLASS_NAME, using ="smallSubmit")WebElement elecreatesubmit;

	@And("Enter the CompanyName as (.*)")
	public CreateLeadsPage entercompanyname(String data)
	{
		type(elecompanyname, data);
		return this;
	}
	@And("Enter the FirstName as (.*)")
	public CreateLeadsPage enterfirstname(String data)
	{
		type(elefirstname, data);
		String text = getText(elefirstname);
		return this;
	}
	@And("Enter the LastName as (.*)")
	public CreateLeadsPage enterlastname(String data)
	{
		type(elelastname, data);
		return this;
	}
	public CreateLeadsPage selectMarketingCampign(String data)
	{
		selectDropDownUsingText(elemarketingcampaignid, data);
		return this;
	}
	public CreateLeadsPage selectSourceID(String data)
	{
		selectDropDownUsingText(eledatasourceid, data);
		return this;
	}
	@And("Click on Create Lead button")
	public ViewLeadsPage clickCreate()
	{
		click(elecreatesubmit);
		return new ViewLeadsPage() ;
	}

	

}