package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {



	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH, using ="(//input[@name='firstName'])[3]")WebElement elefirstname;
	@FindBy(how = How.XPATH, using ="//button[text()='Find Leads']")WebElement elefindlead;
	@FindBy(how = How.XPATH, using ="(//a[@class ='linktext'])[4]")WebElement elefirstleadid;

	@And("Enter the first name as (.*)")
	public FindLeadsPage enterfirstname(String data)
	{
		type(elefirstname, data);
		return this;
	}
	@And ("Click on Find Leads in the Find Lead page")
	public FindLeadsPage clickFindLeads() 
	{

		waitForElement(elefindlead);
		click(elefindlead);
		return this;
	}
	@And ("Click on the first appearing lead in the Lead list")
	public ViewLeadsPage clickFirstLeadId() 
	{
		waitForElement(elefirstleadid);
		click(elefirstleadid);
		return new ViewLeadsPage();
	}


}
