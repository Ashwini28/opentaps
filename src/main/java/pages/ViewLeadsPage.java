package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class ViewLeadsPage extends ProjectMethods {

	public ViewLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "viewLead_firstName_sp")WebElement elefirstnameverify;
	@FindBy(how =How.LINK_TEXT, using ="Edit") WebElement eleedit;

	@And("Verify firstname of the lead created (.*)")
	public ViewLeadsPage verifyFirstName(String data) {		
		verifyPartialText(elefirstnameverify, data);
		return this;
	}
	@And ("Click on Edit in the view lead page")
	public UpdateLeadsPage clickedit() {		
		click(eleedit);
		return new UpdateLeadsPage();
	}
	@And("Verify Lead is created")
	public ViewLeadsPage verifyLeadcreation() {

		System.out.println("Lead is created successfully");
		return this;
	}
	@Then("verify that the lead is updated")
	private ViewLeadsPage leadisUpdated() {
		
		System.out.println("The lead first name is updated");
		return this;

	}
}
