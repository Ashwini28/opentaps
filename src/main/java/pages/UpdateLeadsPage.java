package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class UpdateLeadsPage extends ProjectMethods {

	public UpdateLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "updateLeadForm_firstName")WebElement elefirstnameupdate;
	@FindBy(how =How.CLASS_NAME, using ="smallSubmit") WebElement eleupdate;
	
	@And ("Update the Firstname as (.*)")
	public UpdateLeadsPage updateFirstName(String data) {		
		type(elefirstnameupdate, data);
		return this;
	}
	@And ("Click on Update button")
	public ViewLeadsPage clickUpdate() {		
		click(eleupdate);
		return new ViewLeadsPage();
	}
	
}
