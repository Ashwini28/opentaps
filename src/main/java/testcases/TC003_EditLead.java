package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_EditLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_EditLead";
		testDescription = "EditLead";
		authors = "Ashwini";
		category = "smoke";
		dataSheetName = "EditLead_01";
		testNodes = "Leads";
	}
	
	@Test(dataProvider ="fetchData")
	public void editLead(String username, String pwd, String fname, String updatename) throws InterruptedException
	{
		
		new LoginPage()
		.enterUserName(username)
		.enterPassword(pwd)
		.clickLogin()
		.ClickCrm()
		.clickLeads()
		.clickFindLeads()
		.enterfirstname(fname)
		.clickFindLeads()
		.clickFirstLeadId()
		.clickedit()
		.updateFirstName(updatename)
		.clickUpdate()
		.verifyFirstName(updatename);
		
		
	}

}
