package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "CreateLead";
		authors = "Ashwini";
		category = "smoke";
		dataSheetName = "CreateLead_01";
		testNodes = "Leads";
	}
	@Test(dataProvider="fetchData")
	public void createlead(String username, String pwd, String cname, String fname, String lname, String d1, String d2) {
		
		new LoginPage()
		.enterUserName(username)
		.enterPassword(pwd)
		.clickLogin()
		.VerifyLogin()
		.ClickCrm()
		.clickLeads()
		.clickCreateLeads()
		.entercompanyname(cname)
		.enterfirstname(fname)
		.enterlastname(lname)
		.clickCreate()
		.verifyLeadcreation()
		.verifyFirstName(fname);
		

		
	}

}
