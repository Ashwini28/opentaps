package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
/*@CucumberOptions(features="src/test/java/feature/CreateLeadCucumberIntegration.feature", glue ={"testcase","pages"},monochrome = true
,dryRun =true, snippets =SnippetType.CAMELCASE )*/


@CucumberOptions(features="src/test/java/feature/EditLead.feature"
, glue ={"testcase","pages"},monochrome = true)

public class RunTest 
{

}
