/*package testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {

	public ChromeDriver driver;
	@Given("Open the Browser")
	public void openTheBrowser() {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver = new ChromeDriver();

	}

	@And("Maximize the Browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();

	}

	@And("Set the Timeout")
	public void setTheTimeout() {
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);

	}
	@And("Launch the URL")
	public void LaunchtheURL() {
		driver.get("http://leaftaps.com/opentaps");

	}

	@Given("Enter the UserName as (.*)")
	public void enterTheUserNameAsDemoSalesManager(String data) {

		driver.findElementById("username").sendKeys(data);

	}

	@Given("Enter the Password as (.*)")
	public void enterThePassword(String data) {
		driver.findElementById("password").sendKeys(data);

	}

	@When("Click on Login button")
	public void clickOnLoginButton() {

		driver.findElementByClassName("decorativeSubmit").click();

	}

	@Then("Verify the Login")
	public void verifyTheLogin() {
		System.out.println("The login is successfull");
	}

	@Then("Click on crmsfa in Home page")
	public void clickOnCrmsfaInHomePage() {

		driver.findElementByLinkText("CRM/SFA").click();

	}

	@Then("Click on Leads in The MyHome Page")
	public void clickOnLeads() {
		driver.findElementByLinkText("Leads").click();

	}

	@Then("Click on Create Leads in the MyLeads page")
	public void clickOnCreateLeads() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@Then("Enter the CompanyName as (.*)")
	public void enterTheCompanyNameAsCognizant(String data) {

		driver.findElementById("createLeadForm_companyName").sendKeys(data);

	}

	@Then("Enter the FirstName as (.*)")
	public void enterTheFirstNameAsTestNeon(String data) {

		driver.findElementById("createLeadForm_firstName").sendKeys(data);
		

	}

	@Then("Enter the LastName as (.*)")
	public void enterTheLastNameAsLeaf(String data) {
		driver.findElementById("createLeadForm_lastName").sendKeys(data);
	}

	@When("Click on Create Lead button")
	public void clickOnCreateLeadButton() {
		driver.findElementByClassName("smallSubmit").click();

	}

	@Then("Verify Lead is created")
	public void verifyLeadIsCreated() {
		System.out.println("The Lead is created successfully");

	}

	@Then("Verify firstname of the lead created")
	public void verifyFirstnameOfTheLeadCreated() {
	String text = driver.findElementById("viewLead_firstName_sp").getText();
	if (text.contains("test"))
	{
		System.out.println("Firstname is verified");
	}
	else
		System.out.println("Firstname is not verified");
	}
}


*/