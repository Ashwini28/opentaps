Feature: Edit Leads after Lead Creation

Scenario Outline:Positive Edit Lead 
And Enter the UserName as <username>
And Enter the Password as <password>
When Click on Login button
Then Verify the Login
And Click on crmsfa in Home page
And Click on Leads in The MyHome Page
And Click on Find Leads
And Enter the first name as <firstname>
And Click on Find Leads in the Find Lead page
And Click on the first appearing lead in the Lead list
And Click on Edit in the view lead page
And Update the Firstname as <updatename>
When Click on Update button
Then verify that the lead is updated
Then Verify firstname of the lead created <verifyname>

Examples:
|username|password|firstname|updatename|verifyname|
|DemoSalesManager|crmsfa|TestNeon|testone|test|
|DemoCSR|crmsfa|TestNeon|testtwo|test|

