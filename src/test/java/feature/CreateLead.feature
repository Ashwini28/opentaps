Feature: Create Leads in LeapTaps

Scenario: Positive Lead Creation
Given Open the Browser
And Maximize the Browser
And Set the Timeout
And Launch the URL
And Enter the UserName as DemoSalesManager
And Enter the Password as crmsfa
When Click on Login button
Then Verify the Login
And Click on crmsfa in Home page
And Click on Leads in The MyHome Page
And Click on Create Leads in the MyLeads page
And Enter the CompanyName as Cognizant
And Enter the FirstName as TestNeon1
And Enter the LastName as Leaf
When Click on Create Lead button
Then Verify Lead is created
Then Verify firstname of the lead created


Scenario: Positive Lead Creation
Given Open the Browser
And Maximize the Browser
And Set the Timeout
And Launch the URL
And Enter the UserName as DemoCSR
And Enter the Password as crmsfa
When Click on Login button
Then Verify the Login
And Click on crmsfa in Home page
And Click on Leads in The MyHome Page
And Click on Create Leads in the MyLeads page
And Enter the CompanyName as Cognizant
And Enter the FirstName as TestNeon5
And Enter the LastName as Leaf
When Click on Create Lead button
Then Verify Lead is created
Then Verify firstname of the lead created