Feature: Create Leads in LeapTaps

#Background: 
#Given Open the Browser
#And Maximize the Browser
#And Set the Timeout
#And Launch the URL


Scenario Outline: Positive Lead Creation
And Enter the UserName as <username>
And Enter the Password as <password>
When Click on Login button
Then Verify the Login
And Click on crmsfa in Home page
And Click on Leads in The MyHome Page
And Click on Create Leads in the MyLeads page
And Enter the CompanyName as <CompanyName>
And Enter the FirstName as <FirstName>
And Enter the LastName as <LastName>
When Click on Create Lead button
Then Verify Lead is created
Then Verify firstname of the lead created <verifyname>

Examples:
|username|password|CompanyName|FirstName|LastName|verifyname|
|DemoSalesManager|crmsfa|Amazon|TestNeon1|leaf|test|
|DemoCSR|crmsfa|Amazon|TestNeon2|leaf|test|


Scenario: Positive Lead Creation
And Click on Leads in The MyHome Page
And Click on Create Leads in the MyLeads page
And Enter the CompanyName as Cognizant
And Enter the FirstName as TestNeon5
And Enter the LastName as Leaf
When Click on Create Lead button
Then Verify Lead is created
Then Verify firstname of the lead created <verifyname>